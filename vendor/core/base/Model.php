<?php

namespace vendor\core\base;

use \Valitron\Validator;
use vendor\core\Db;

require 'vendor/vlucas/valitron/src/Valitron/Validator.php';

abstract class Model {
	
	protected $pdo;
	protected $table;
	protected $attributes = [];
	public $errors = [];
	public $rules = [];
	
	public function __construct() {
		$this->pdo = Db::instance();
	}
	
	public function load($data) {
		foreach ($this->attributes as $name => $value) {
			if(isset($data[$name])) {
				$this->attributes[$name] = $data[$name];
			}
		}
	
	}
	
	public function validate($data) {
		$v = new Validator($data);
		$v->rules($this->rules);
		if($v->validate()) {
			return TRUE;
		}
		$this->errors = $v->errors();
		return FALSE;
	}
	
	public function save($table) {
		$tbl = \R::dispense($table);
		foreach ($this->attributes as $name => $value) {
			$tbl->$name = $value;
		}
		return \R::store($tbl);
	}
	
	public function getErrors() {
		$errors = '<ul>';
		foreach ($this->errors as $error) {
			foreach ($error as $item) {
				$errors .= "<li>$item</li>";
			}
		}
		$errors .= '</ul>';
		$_SESSION['error'] = $errors;
	}
	
	public function query($sql) {
		return $this->pdo->execute($sql);
	}
	
	public function findAll() {
	  $sql = "SELECT * FROM {$this->table}";
	  return $this->pdo->query($sql);
	}
	
}