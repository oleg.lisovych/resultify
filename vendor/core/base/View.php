<?php

namespace vendor\core\base;

/**
 * Class View
 *
 * @package vendor\core\base
 */
class View {
	
	/**
	 * @var array
	 */
		public $route = [];
	
	/**
	 * @var
	 */
		public $view;
	
	/**
	 * @var
	 */
		public $layout;
	
	/**
	 * View constructor.
	 *
	 * @param $route
	 * @param string $layout
	 * @param $view
	 */
		public function __construct($route, $layout = '', $view = '') {
			$this->route = $route;
			$this->layout = $layout ?: LAYOUT;
			$this->view = $view;
		}
		
		public function render($vars) {
			extract($vars);
			$file_view = APP . "/views/{$this->route['controller']}/{$this->view}.php";
			ob_start();
			if(is_file($file_view)) {
				require $file_view;
			}
			else {
				echo "Cant find $file_view view";
			}
			$content = ob_get_clean();
			
			$file_layout = APP . "/views/layouts/{$this->layout}.php";
			if(is_file($file_layout)) {
				require $file_layout;
			}
			else {
				echo "Cant find $file_layout view";
			}
		}
}