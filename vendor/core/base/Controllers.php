<?php

namespace vendor\core\base;

/**
 * Class Controllers
 *
 * @package vendor\core\base
 */
abstract class Controllers {
	
	/**
	 * @var array
	 */
	public $route = [];
	
	/**
	 * @var
	 */
	public $view;
	
	/**
	 * @var
	 */
	public $layout;
	
	/**
	 * @var
	 */
	public $vars = [];
	
	/**
	 * Controllers constructor.
	 *
	 * @param $route
	 */
	public function __construct($route) {
		session_start();
		$this->route = $route;
		$this->view = $route['action'];
		
	}
	
	public function getView() {
		$vObj = new View($this->route, $this->layout, $this->view);
		$vObj->render($this->vars);
	}
	
	/**
	 * @param $vars
	 */
	public function set($vars) {
		$this->vars = $vars;
}

}