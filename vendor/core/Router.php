<?php

namespace vendor\core;

/**
 * Class Router
 *
 * @package vendor\core
 */
class Router {
	
	protected static $routes = [];
	protected static $route = [];
	
	/**
	 * @param $regexp
	 * @param array $route
	 */
	public static function add($regexp, $route = []) {
		self::$routes[$regexp] = $route;
	}
	
	/**
	 * @return array
	 */
	public static function getRoutes() {
		return self::$routes;
	}
	
	/**
	 * @return array
	 */
	public static function getRoute() {
		return self::$route;
	}
	
	/**
	 * @param $url
	 *
	 * @return bool
	 */
	public static function matchRoute($url) {
		foreach(self::$routes as $pattern => $route) {
			if(preg_match("#$pattern#i", $url, $matches)) {
				foreach($matches as $k => $v) {
					if(is_string($k)) {
						$route[$k] = $v;
					}
				}
				if(!isset($route['action'])) {
					$route['action'] = 'index';
				}
				$route['controller'] = self::upperCamelCase($route['controller']);
				self::$route = $route;
				return true;
			}
		}
		return FALSE;
	}
	
	/**
	 * @param $url
	 */
	public static function dispatch($url) {
		if(self::matchRoute($url)) {
			$controller = 'app\controllers\\' . self::$route['controller'] . 'Controller';
			if(class_exists($controller)) {
				$cObj = new $controller(self::$route);
				$action = self::lowerCamelCase(self::$route['action']) . 'Action';
				if(method_exists($cObj, $action)) {
					$cObj->$action();
					$cObj->getView();
				}
				else {
					echo "Method does not exist";
				}
			}
			else {
				echo "$controller does not exist";
			}

		}else {
			//todo: create correct errors view
			echo '404';
			http_response_code(404);
		}
	}
	
	/**
	 * @param $name
	 *
	 * @return mixed
	 */
	public static function upperCamelCase($name) {
		return $name = str_replace( ' ', '', ucwords(str_replace('-', ' ', $name)));
	}
	
	/**
	 * @param $name
	 *
	 * @return string
	 */
	public static function lowerCamelCase($name) {
		return lcfirst(self::upperCamelCase($name));
	}

}
