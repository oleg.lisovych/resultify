<?php

namespace app\model;

use vendor\core\base\Model;

/**
 * Class Books
 *
 * @package app\model
 */
class Books extends Model {

	public $table = 'book';

}