<?php

namespace app\model;

use vendor\core\base\Model;

/**
 * Class User
 *
 * @package app\model
 */
class User extends Model {
	
	public $attributes = [
		'username' => '',
		'email' => '',
		'password' => '',
		];
	
	public $rules = [
		'required' => [
			['username'],
			['email'],
			['password']
		],
		'email' => [
			['email']
		],
		'lengthMin' => [
			['password', 6]
			]
	];
	
	public function checkUnique() {
		$user = \R::findOne('user', 'login = ? OR email = ? LIMIT 1', $this->attributes['login'], $this->attributes['email']);
		if($user) {
			if($user->login == $this->attributes['login']) {
				$this->errors['unique'][] = 'User exists';
			}
			if($user->email == $this->attributes['email']) {
				$this->errors['unique'][] = 'Email exists';
			}
			return FALSE;
		}
		return TRUE;
	}

}