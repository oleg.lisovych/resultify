<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <title>Test task</title>
    <link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css" />
    <link href="http://fonts.googleapis.com/css?family=Kreon" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="/public/css/style.css" />
    <script src="/js/jquery-1.6.2.js" type="text/javascript"></script>
    <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet" id="bootstrap-css">
</head>
<body>
<div id="wrapper">
    <div id="header">
        <div id="menu">
            <ul>
                <li class="first active"><a href="/">Main</a></li>
                <li><a href="/books">Books</a></li>
                <li><a href="/user/signup">Signup</a></li>
                <li><a href="/user/login">Login</a></li>
                <li><a href="/user/logout">Logout</a></li>
            </ul>
            <br class="clearfix" />
        </div>
    </div>
    <div id="page">
        <div id="content">
            <div class="box">
	            <?php if(isset($_SESSION['error'])) : ?>
                  <div class="alert alert-danger">
				     <?=$_SESSION['error']; unset($_SESSION['error']) ?>
                  </div>
	            <?php endif; ?>
	            <?php if(isset($_SESSION['success'])) : ?>
                  <div class="alert alert-success">
				      <?=$_SESSION['success']; unset($_SESSION['success']) ?>
                  </div>
	            <?php endif; ?>
	            <?php print $content; ?>
            </div>
            <br class="clearfix" />
        </div>
        <br class="clearfix" />
    </div>
    <div id="page-bottom">
        <div id="page-bottom-sidebar">
            <h3>Contacts</h3>
            <ul class="list">
                <li>skypeid: Vasya Pupkin</li>
                <li class="last">email: vasyapupkin@gmail.com</li>
            </ul>
        </div>
        <div id="page-bottom-content">
            <h3>About us</h3>
            <p>
                It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal
                distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.
            </p>
        </div>
        <br class="clearfix" />
    </div>
</div>
<div id="footer">

</div>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="/bootstrap/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>