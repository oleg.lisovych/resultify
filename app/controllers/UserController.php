<?php

namespace app\controllers;

use app\model\User;
use vendor\core\base\Controllers;

/**
 * Class UserController
 *
 * @package app\controllers
 */
class UserController extends Controllers {
	
	public function signupAction() {
		if(!empty($_POST)) {
			$user = new User();
			$data = $_POST;
			$user->load($data);
			if(!$user->validate($data)) {
				$user->getErrors();
				$_SESSION['form_data'] = $data;
				redirect();
			}
			if($user->save('users')) {
				$_SESSION['success'] = 'Welcome';
				redirect('/');
			}
			else {
				$_SESSION['error'] = 'Try again';
				redirect();
			}
		}
	}
	
	public function loginAction() {
	
	}
	
	public function logoutAction() {
		return redirect("/user/login");
	}

}