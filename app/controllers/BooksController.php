<?php

namespace app\controllers;

use app\model\Books;
use vendor\core\base\Controllers;

/**
 * Class BooksController
 *
 * @package app\controllers
 */
class BooksController extends Controllers {
	
	public function indexAction() {
		$model = new Books;
		$books = $model->findAll();
		$this->set(compact( 'books'));
	}
	
}